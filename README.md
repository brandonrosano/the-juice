# The Juice

The Juice is a script meant to be ran with crontab every 15 seconds in the background preventing the user from being able to look at porn with alternative browsers.

# Below is the layout for crontab to run every 15 seconds

1 * * * * * /usr/bin/the_juice\
2 * * * * * sleep 15; /usr/bin/the_juice\
3 * * * * * sleep 30; /usr/bin/the_juice\
4 * * * * * sleep 45; /usr/bin/the_juice
